// var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp

// //openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path

// (async() => {
// var options, encrypted;

// options = {
//     message: openpgp.message.fromBinary(new Uint8Array([0x01, 0x01, 0x01])), // input as Message object
//     passwords: ['secret stuff'],                                             // multiple passwords possible
//     armor: false                                                             // don't ASCII armor (for Uint8Array output)
// };

// await openpgp.encrypt(options).then(function(ciphertext) {
//     encrypted = ciphertext.message.packets.write(); // get raw encrypted packets as Uint8Array
//     //console.log(options.message.packets[0].data)
// });

// options = {
//     message: await openpgp.message.read(encrypted), // parse encrypted bytes
//     passwords: ['secret stuff'],              // decrypt with password
//     format: 'binary'                          // output as Uint8Array
// };

// openpgp.decrypt(options).then(function(plaintext) {
//     return plaintext.data // Uint8Array([0x01, 0x01, 0x01])
// });
// })();

const openpgp = require('openpgp') // use as CommonJS, AMD, ES6 module or via window.openpgp
 
openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path

// put keys in backtick (``) to avoid errors caused by spaces or tabs
const pubkey = ['-----BEGIN PGP PUBLIC KEY BLOCK-----',
'Version: GnuPG v2.0.19 (GNU/Linux)',
'',
'mI0EUmEvTgEEANyWtQQMOybQ9JltDqmaX0WnNPJeLILIM36sw6zL0nfTQ5zXSS3+',
'fIF6P29lJFxpblWk02PSID5zX/DYU9/zjM2xPO8Oa4xo0cVTOTLj++Ri5mtr//f5',
'GLsIXxFrBJhD/ghFsL3Op0GXOeLJ9A5bsOn8th7x6JucNKuaRB6bQbSPABEBAAG0',
'JFRlc3QgTWNUZXN0aW5ndG9uIDx0ZXN0QGV4YW1wbGUuY29tPoi5BBMBAgAjBQJS',
'YS9OAhsvBwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AACgkQSmNhOk1uQJQwDAP6',
'AgrTyqkRlJVqz2pb46TfbDM2TDF7o9CBnBzIGoxBhlRwpqALz7z2kxBDmwpQa+ki',
'Bq3jZN/UosY9y8bhwMAlnrDY9jP1gdCo+H0sD48CdXybblNwaYpwqC8VSpDdTndf',
'9j2wE/weihGp/DAdy/2kyBCaiOY1sjhUfJ1GogF49rC4jQRSYS9OAQQA6R/PtBFa',
'JaT4jq10yqASk4sqwVMsc6HcifM5lSdxzExFP74naUMMyEsKHP53QxTF0Grqusag',
'Qg/ZtgT0CN1HUM152y7ACOdp1giKjpMzOTQClqCoclyvWOFB+L/SwGEIJf7LSCEr',
'woBuJifJc8xAVr0XX0JthoW+uP91eTQ3XpsAEQEAAYkBPQQYAQIACQUCUmEvTgIb',
'LgCoCRBKY2E6TW5AlJ0gBBkBAgAGBQJSYS9OAAoJEOCE90RsICyXuqIEANmmiRCA',
'SF7YK7PvFkieJNwzeK0V3F2lGX+uu6Y3Q/Zxdtwc4xR+me/CSBmsURyXTO29OWhP',
'GLszPH9zSJU9BdDi6v0yNprmFPX/1Ng0Abn/sCkwetvjxC1YIvTLFwtUL/7v6NS2',
'bZpsUxRTg9+cSrMWWSNjiY9qUKajm1tuzPDZXAUEAMNmAN3xXN/Kjyvj2OK2ck0X',
'W748sl/tc3qiKPMJ+0AkMF7Pjhmh9nxqE9+QCEl7qinFqqBLjuzgUhBU4QlwX1GD',
'AtNTq6ihLMD5v1d82ZC7tNatdlDMGWnIdvEMCv2GZcuIqDQ9rXWs49e7tq1NncLY',
'hz3tYjKhoFTKEIq3y3Pp',
'=h/aX',
'-----END PGP PUBLIC KEY BLOCK-----'].join('\n');

const privkey = ['-----BEGIN PGP PRIVATE KEY BLOCK-----',
'Version: GnuPG v2.0.19 (GNU/Linux)',
'',
'lQH+BFJhL04BBADclrUEDDsm0PSZbQ6pml9FpzTyXiyCyDN+rMOsy9J300Oc10kt',
'/nyBej9vZSRcaW5VpNNj0iA+c1/w2FPf84zNsTzvDmuMaNHFUzky4/vkYuZra//3',
'+Ri7CF8RawSYQ/4IRbC9zqdBlzniyfQOW7Dp/LYe8eibnDSrmkQem0G0jwARAQAB',
'/gMDAu7L//czBpE40p1ZqO8K3k7UejemjsQqc7kOqnlDYd1Z6/3NEA/UM30Siipr',
'KjdIFY5+hp0hcs6EiiNq0PDfm/W2j+7HfrZ5kpeQVxDek4irezYZrl7JS2xezaLv',
'k0Fv/6fxasnFtjOM6Qbstu67s5Gpl9y06ZxbP3VpT62+Xeibn/swWrfiJjuGEEhM',
'bgnsMpHtzAz/L8y6KSzViG/05hBaqrvk3/GeEA6nE+o0+0a6r0LYLTemmq6FbaA1',
'PHo+x7k7oFcBFUUeSzgx78GckuPwqr2mNfeF+IuSRnrlpZl3kcbHASPAOfEkyMXS',
'sWGE7grCAjbyQyM3OEXTSyqnehvGS/1RdB6kDDxGwgE/QFbwNyEh6K4eaaAThW2j',
'IEEI0WEnRkPi9fXyxhFsCLSI1XhqTaq7iDNqJTxE+AX2b9ZuZXAxI3Tc/7++vEyL',
'3p18N/MB2kt1Wb1azmXWL2EKlT1BZ5yDaJuBQ8BhphM3tCRUZXN0IE1jVGVzdGlu',
'Z3RvbiA8dGVzdEBleGFtcGxlLmNvbT6IuQQTAQIAIwUCUmEvTgIbLwcLCQgHAwIB',
'BhUIAgkKCwQWAgMBAh4BAheAAAoJEEpjYTpNbkCUMAwD+gIK08qpEZSVas9qW+Ok',
'32wzNkwxe6PQgZwcyBqMQYZUcKagC8+89pMQQ5sKUGvpIgat42Tf1KLGPcvG4cDA',
'JZ6w2PYz9YHQqPh9LA+PAnV8m25TcGmKcKgvFUqQ3U53X/Y9sBP8HooRqfwwHcv9',
'pMgQmojmNbI4VHydRqIBePawnQH+BFJhL04BBADpH8+0EVolpPiOrXTKoBKTiyrB',
'UyxzodyJ8zmVJ3HMTEU/vidpQwzISwoc/ndDFMXQauq6xqBCD9m2BPQI3UdQzXnb',
'LsAI52nWCIqOkzM5NAKWoKhyXK9Y4UH4v9LAYQgl/stIISvCgG4mJ8lzzEBWvRdf',
'Qm2Ghb64/3V5NDdemwARAQAB/gMDAu7L//czBpE40iPcpLzL7GwBbWFhSWgSLy53',
'Md99Kxw3cApWCok2E8R9/4VS0490xKZIa5y2I/K8thVhqk96Z8Kbt7MRMC1WLHgC',
'qJvkeQCI6PrFM0PUIPLHAQtDJYKtaLXxYuexcAdKzZj3FHdtLNWCooK6n3vJlL1c',
'WjZcHJ1PH7USlj1jup4XfxsbziuysRUSyXkjn92GZLm+64vCIiwhqAYoizF2NHHG',
'hRTN4gQzxrxgkeVchl+ag7DkQUDANIIVI+A63JeLJgWJiH1fbYlwESByHW+zBFNt',
'qStjfIOhjrfNIc3RvsggbDdWQLcbxmLZj4sB0ydPSgRKoaUdRHJY0S4vp9ouKOtl',
'2au/P1BP3bhD0fDXl91oeheYth+MSmsJFDg/vZJzCJhFaQ9dp+2EnjN5auNCNbaI',
'beFJRHFf9cha8p3hh+AK54NRCT++B2MXYf+TPwqX88jYMBv8kk8vYUgo8128r1zQ',
'EzjviQE9BBgBAgAJBQJSYS9OAhsuAKgJEEpjYTpNbkCUnSAEGQECAAYFAlJhL04A',
'CgkQ4IT3RGwgLJe6ogQA2aaJEIBIXtgrs+8WSJ4k3DN4rRXcXaUZf667pjdD9nF2',
'3BzjFH6Z78JIGaxRHJdM7b05aE8YuzM8f3NIlT0F0OLq/TI2muYU9f/U2DQBuf+w',
'KTB62+PELVgi9MsXC1Qv/u/o1LZtmmxTFFOD35xKsxZZI2OJj2pQpqObW27M8Nlc',
'BQQAw2YA3fFc38qPK+PY4rZyTRdbvjyyX+1zeqIo8wn7QCQwXs+OGaH2fGoT35AI',
'SXuqKcWqoEuO7OBSEFThCXBfUYMC01OrqKEswPm/V3zZkLu01q12UMwZach28QwK',
'/YZly4ioND2tdazj17u2rU2dwtiHPe1iMqGgVMoQirfLc+k=',
'=lw5e',
'-----END PGP PRIVATE KEY BLOCK-----'].join('\n'); //encrypted private key

const passphrase = `hello world` //what the privKey is encrypted with

const encryptDecryptFunction = async() => {

    var options, encrypted, privkey2, pubkey2, privkey3, pubkey3
    let loading = true;
    var keyring2 = new openpgp.Keyring()

    // creating the first public and private key
    const pubKeyObj = (await openpgp.key.readArmored(pubkey)).keys[0]
    const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0]
    await privKeyObj.decrypt(passphrase)
    // end of creating first private and public keys

    // makes publicKeys field in keyring available for use
    // prevents loading the same keys multiple when restarting this node appl
    async function load() {
        if (loading){
            await keyring2.load();
            await keyring2.clear();
            await keyring2.store()
            loading = false;
        }
    }

    // used to import public keys into the keyring
    async function importPublicKey(key){
        await load()
        await keyring2.publicKeys.importKey(key);
        return (await openpgp.key.readArmored(key)).keys[0].primaryKey.getFingerprint();
    }

    //info used to create the second key pair
    options = {
        userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
        curve: "ed25519",                                         // ECC curve name
        passphrase: `hello world`         // protects the private key
    };
    
    //creating the second private and public keys
    await openpgp.generateKey(options).then(function(key) {
        privkey2 = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
        pubkey2 = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        return privkey2, pubkey2
    });
    //end of creating the second private and public keys



    // imports the public keys into the keyring
    // only importing pubkey and pubkey2, NOT pubkey3
    await importPublicKey(pubkey)
    await importPublicKey(pubkey2);



    // ******uncomment this to view all of the public keys in the keyring******
    // should only be 2 at this time
    // console.log(keyring2.publicKeys.keys)


    const privKeyObj2 = (await openpgp.key.readArmored(privkey2)).keys[0]
    const pubKeyObj2 = (await openpgp.key.readArmored(pubkey2)).keys[0]
    await privKeyObj2.decrypt(passphrase)


    //info used to create the third key pair
    options = {
        userIds: [{ name:'test', email:'test@example.com' }], // multiple user IDs
        curve: "p256",                                         // ECC curve name
        passphrase: `hello world`         // protects the private key
    };

    //creating the third private and public keys
    await openpgp.generateKey(options).then(function(key) {
        privkey3 = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
        pubkey3 = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        return privkey3, pubkey3
    });
    //end of creating the third private and public keys


    const privKeyObj3 = (await openpgp.key.readArmored(privkey3)).keys[0]
    const pubKeyObj3 = (await openpgp.key.readArmored(pubkey3)).keys[0]
    await privKeyObj3.decrypt(passphrase)

    // this is where you define what you want to be encrypted
    // and what public key you are using to encrypt it
    options = {
        message: openpgp.message.fromText('Hello, World!'),       // input as Message object
        publicKeys: keyring2.publicKeys.keys                                 // for encryption
        //  privateKeys: [privKeyObj2]                                 // for signing (optional)
    }
    
    // where you are actually encrypting the message
    await openpgp.encrypt(options).then(ciphertext => {
        encrypted = ciphertext.data // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
    })



    // this is where you define what you want to be decrypted
    // and what private keys you are using to decrypt the message
    options = {
        message: await openpgp.message.readArmored(encrypted),          // parse armored message
        // publicKeys: (openpgp.key.readArmored(pubkey)).keys,          // for verification (optional)
        
        // privKeyObj and privKeyObj2 are able to decrpyt the message
        // to test for failure, remove privKeyObj1 and privKeyObj2
        // And put in privKeyObj3. Will have an error message that says
        // 'Error decrypting message'
        privateKeys: [privKeyObj, privKeyObj2]                                 // for decryption
    }
    
    // where the decryption is actually happening
    openpgp.decrypt(options).then(plaintext => {
        console.log(plaintext.data)
        return plaintext.data // 'Hello, World!'
    })
    
}

encryptDecryptFunction()